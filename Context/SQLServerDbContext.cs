using System.IO; 
using Lib.Infra.Data.Base.Context; 
using Microsoft.EntityFrameworkCore; 
using Microsoft.Extensions.Configuration; 
  
namespace Lib.Infra.Data.SQL.Context 
{ 
    public abstract class SQLServerDbContext : BaseDbContext 
    { 
        public SQLServerDbContext(DbContextOptions options)  
            : base(options) 
        { 
  
        } 
  
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        { 
            var config = new ConfigurationBuilder() 
                                .SetBasePath(Directory.GetCurrentDirectory()) 
                                .AddJsonFile("appsettings.json") 
                                .Build(); 
  
            optionsBuilder.UseSqlServer(config.GetConnectionString("Cabeleireiro")); 
        }  
    } 
} 
